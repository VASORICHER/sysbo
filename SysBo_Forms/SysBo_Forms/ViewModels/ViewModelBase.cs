﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Xamarin.Forms;
using SysBo_Forms.Services;
using Prism.Services;

namespace SysBo_Forms.ViewModels
{
    public class ViewModelBase : BindableBase
    {
        public IErecap_Service service_Erecap;
        public ISysbo_Service service_Sysbo;
        public INavigationService _navigationService;
        public IPageDialogService _pageDialog;

        public ViewModelBase(INavigationService navigationService, IPageDialogService pageDialog, IErecap_Service _serviceErecap, ISysbo_Service _serviceSysbo)
        {
            _navigationService = navigationService;
            service_Erecap = _serviceErecap;
            service_Sysbo = _serviceSysbo;
            _pageDialog = pageDialog;
        }

        private bool _IsBusy;
        public bool IsBusy
        {
            get => _IsBusy;
            set => SetProperty(ref _IsBusy, value);
        }
        
        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { SetProperty(ref _Title, value); }
        }

        private string _Usuario;

        public string Usuario
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("Usuario"))
                    _Usuario = (string)Application.Current.Properties["Usuario"];
                return _Usuario;
            }
            set
            {
                Application.Current.Properties["Usuario"] = value;
                SetProperty(ref _Usuario, value);
            }
        }

        private string _Nombre;

        public string Nombre
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("_Nombre"))
                    _Nombre = (string)Application.Current.Properties["_Nombre"];
                return _Nombre;
            }
            set
            {
                Application.Current.Properties["_Nombre"] = value;
                SetProperty(ref _Nombre, value);
            }
        }

        private string _Clave;

        public string Clave
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("Clave"))
                    _Clave = (string)Application.Current.Properties["Clave"];
                return _Clave;
            }
            set
            {
                Application.Current.Properties["Clave"] = value;
                SetProperty(ref _Clave, value);
            }
        }

        private string _Entrada;

        public string Entrada
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("Entrada"))
                    _Entrada = (string)Application.Current.Properties["Entrada"];
                return _Entrada;
            }
            set
            {
                Application.Current.Properties["Entrada"] = value;
                SetProperty(ref _Entrada, value);
            }
        }

        private string _Tipo;
        public string Tipo
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("Tipo"))
                    _Tipo = (string)Application.Current.Properties["Tipo"];
                return _Tipo;
            }
            set
            {
                Application.Current.Properties["Tipo"] = value;
                SetProperty(ref _Tipo, value);
            }
        }
        private int _LimiteEntrada;
        public int LimiteEntrada
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("_LimiteEntrada"))
                    _LimiteEntrada = (int)Application.Current.Properties["_LimiteEntrada"];
                return _LimiteEntrada;
            }
            set
            {
                Application.Current.Properties["_LimiteEntrada"] = value;
                SetProperty(ref _LimiteEntrada, value);
            }
        }
        private int _LimiteRevision;
        public int LimiteRevision
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("_LimiteRevision"))
                    _LimiteRevision = (int)Application.Current.Properties["_LimiteRevision"];
                return _LimiteRevision;
            }
            set
            {
                Application.Current.Properties["_LimiteRevision"] = value;
                SetProperty(ref _LimiteRevision, value);
            }
        }
        private int _LimiteEmbarque;
        public int LimiteEmbarque
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("_LimiteEmbarque"))
                    _LimiteEmbarque = (int)Application.Current.Properties["_LimiteEmbarque"];
                return _LimiteEmbarque;
            }
            set
            {
                Application.Current.Properties["_LimiteEmbarque"] = value;
                SetProperty(ref _LimiteEmbarque, value);
            }
        }
        private int _LimiteSalida;
        public int LimiteSalida
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("_LimiteSalida"))
                    _LimiteSalida = (int)Application.Current.Properties["_LimiteSalida"];
                return _LimiteSalida;
            }
            set
            {
                Application.Current.Properties["_LimiteSalida"] = value;
                SetProperty(ref _LimiteSalida, value);
            }
        }
    }
}
