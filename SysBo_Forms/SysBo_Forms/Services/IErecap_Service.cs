﻿using System;
using System.Collections.Generic;
using SysBo_Forms.Model;

namespace SysBo_Forms.Services
{
    public interface IErecap_Service
    {
        event EventHandler<GenericEventArgs<ValueResult<GeneralInfo>>> ValidarUsuario_Completed;
        event EventHandler<GenericEventArgs<ValueResult<IList<byte[]>>>> ObtenerFotos_Completed;
        event EventHandler<GenericEventArgs<ValueResult<CountFotos>>> ObtenerCountFotos_Completed;
        event EventHandler<GenericEventArgs<ValueResult<FotosInfo>>> SubirFotos_Completed;

        void ValidarUsuario(string _usuario, string _clave);
        void ObtenerFotos(string _entrada, string _tipo);
        void ObtenerCountFotos(string _entrada);
        void SubirFotos(string _entrada, string _tipo, byte[] _imageBytes, string _usuario);
    }
}
