﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysBo_Forms.Model
{
    public class RegistroEntrada
    {
        public string Entrada { get; set; }
        public BodegaInfo Bodega { get; set; }
        public ClienteInfo Cliente { get; set; }
        public int Cantidad { get; set; }
        public string Guia { get; set; }
        public string Ref { get; set; }
        public string Seccion { get; set; }
        public string Usuario { get; set; }
    }
}
