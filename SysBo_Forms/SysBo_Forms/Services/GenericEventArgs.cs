﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysBo_Forms.Services
{
    public class GenericEventArgs<T> : EventArgs
    {
        public T Result { get; private set; }
        public GenericEventArgs(T _result)
        {
            Result = _result;
        }
    }
}
