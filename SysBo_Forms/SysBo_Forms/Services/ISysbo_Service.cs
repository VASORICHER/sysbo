﻿using System;
using System.Collections.Generic;
using SysBo_Forms.Model;

namespace SysBo_Forms.Services
{
    public interface ISysbo_Service
    {
        event EventHandler<GenericEventArgs<ValueResult<List<BodegaInfo>>>> catalogo_bodegas_Completed;
        event EventHandler<GenericEventArgs<ValueResult<List<ClienteInfo>>>> catalago_clientes_Completed;
        event EventHandler<GenericEventArgs<ValueResult<RegistroEntrada>>> informacion_entrada_Completed;
        event EventHandler<GenericEventArgs<ValueResult<obj_commit>>> GuardarInfo_Completed;
        event EventHandler<GenericEventArgs<ValueResult<obj_commit>>> GenerarEntrada_Completed;

        void ObtenerCatalagoBodegas();
        void ObtenerCatalagoClientes(string id_bodega);
        void ObtenerInfoEntrada(string id_entrada);
        void GuardarInfo(RegistroEntrada entradaInfo);
        void GenerarEntrada();
    }
}
