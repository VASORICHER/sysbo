﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysBo_Forms.Model
{
    public class FotosInfo
    {
        public CountFotos FotosExistentes { get; set; }
        public IList<byte[]> ListaFotosBytes { get; set; }
        public EntradaInfo infoEntrada { get; set; }
    }
}
