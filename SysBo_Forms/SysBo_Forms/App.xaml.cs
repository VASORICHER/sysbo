﻿using Prism;
using Prism.Ioc;
using Prism.Plugin.Popups;
using SysBo_Forms.Media;
using SysBo_Forms.ViewModels;
using SysBo_Forms.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace SysBo_Forms
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            await NavigationService.NavigateAsync("NavigationPage/Acceso");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterPopupNavigationService();
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<AccesoView, AccesoViewModel>("Acceso");
            containerRegistry.RegisterForNavigation<BusquedaView, BusquedaViewModel>("Busqueda");
            containerRegistry.RegisterForNavigation<OpcionesView, OpcionesViewModel>("Opciones");
            containerRegistry.RegisterForNavigation<GaleriaView, GaleriaViewModel>("Galeria");
            containerRegistry.RegisterForNavigation<ScannerMedia>("scaner");
            containerRegistry.RegisterForNavigation<MenuView, MenuViewModel>("Menu");
            containerRegistry.RegisterForNavigation<RegistroEntradasView, EntradasViewModel>("Entradas");
            containerRegistry.RegisterForNavigation<BasePage>("Base");
            containerRegistry.RegisterForNavigation<BodegaPopup, BodegaPopupViewModel>("PopupBodega");
            containerRegistry.RegisterForNavigation<ClientePopup, ClientePopupViewModel>("PopupCliente");
        }
    }
}
