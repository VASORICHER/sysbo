﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysBo_Forms.Model
{
   public class EntradaInfo
    {
        public string ClienteBodega { get; set; }
        public decimal BultosCount { get; set; }
    }
}
