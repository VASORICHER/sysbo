﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysBo_Forms.Model
{
    public class ClienteInfo
    {
        public string id_cliente { get; set; }
        public string razon_social { get; set; }
        public string rfc { get; set; }
        public string FullName { get; set; }
    }
}
