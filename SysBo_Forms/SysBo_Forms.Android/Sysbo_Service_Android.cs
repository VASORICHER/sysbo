﻿using System;
using SysBo_Forms.Droid;
using SysBo_Forms.Model;
using SysBo_Forms.Services;
using Xamarin.Forms;
using SysBo_Forms.Droid.SYSBO_WS;
using System.Collections.Generic;
using System.Linq;

namespace SysBo_Forms.Droid
{
    public class Sysbo_Service_Android : ISysbo_Service
    {
        readonly SYSBO service;
        public Sysbo_Service_Android()
        {
            service = new SYSBO();
            service.abc_entradaCompleted += (sender, e) =>
              {
                  ValueResult<Model.obj_commit> result = new ValueResult<Model.obj_commit>();
                  if (e.Result.ErrorNumber == "NO DATA")
                      result.Estatus = true;
                  else
                      result.Message = e.Result.ErrorMessage;
                  GuardarInfo_Completed?.Invoke(this, new GenericEventArgs<ValueResult<Model.obj_commit>>(result));

              };
            service.catalogo_bodegasCompleted += (sender, e) =>
            {
                ValueResult<List<BodegaInfo>> resultLista = new ValueResult<List<BodegaInfo>>();
                var ls = new List<BodegaInfo>();
                
                if(e.Result.Length>1)
                {
                    var bodegas = e.Result?.ToList() ?? new List<obj_bodegas>();
                    foreach (var bodega in e.Result)
                    {
                        var infoBodega = new BodegaInfo()
                        {
                            idBodega = bodega.id_bodega,
                            Descripcion = bodega.razon_social
                        };
                        ls.Add(infoBodega);
                    }
                    resultLista.Result = ls;
                    resultLista.Estatus = true;

                    catalogo_bodegas_Completed?.Invoke(this, new GenericEventArgs<ValueResult<List<BodegaInfo>>>(resultLista));
                }
            };
            service.catalogo_clientesCompleted += (sender, e) =>
            {
                if(e.Result.Length>1)
                {
                    ValueResult<List<ClienteInfo>> resultLista = new ValueResult<List<ClienteInfo>>();
                    var ls = new List<ClienteInfo>();
                    foreach (var cliente in e.Result)
                    {
                        var infoCliente = new ClienteInfo()
                        {
                            id_cliente = cliente.id_cliente,
                            razon_social = cliente.razon_social,
                            rfc = cliente.rfc
                        };
                        ls.Add(infoCliente);
                    }
                    resultLista.Result = ls;
                    resultLista.Estatus = true;

                    catalago_clientes_Completed?.Invoke(this, new GenericEventArgs<ValueResult<List<ClienteInfo>>>(resultLista));
                }
            };
            service.informacion_entradaCompleted += (sender, e) =>
            {
                if(e.Result.Length>0)
                {
                    ValueResult<RegistroEntrada> valueResult = new ValueResult<RegistroEntrada>();
                    foreach (var item in e.Result)
                    {
                        if (item.ErrorNumber != "1")
                        {
                            string[] bodegaArray = item.bodega.Split('|');
                            var bodegaInfo = new Model.BodegaInfo()
                            {
                                idBodega = bodegaArray[0],
                                Descripcion = bodegaArray[1]
                            };
                            string[] clienteArray = item.cliente.Split('|');
                            var clienteInfo = new ClienteInfo()
                            {
                                id_cliente = clienteArray[0],
                                razon_social = clienteArray[1]
                            };
                            var entradaInfo = new Model.RegistroEntrada()
                            {
                                Entrada = item.entrada,
                                Bodega = bodegaInfo,
                                Cliente = clienteInfo,
                                Cantidad = Convert.ToInt32(item.cantidad),
                                Guia = item.guia_flete,
                                Ref = item.ref_ups,
                                Seccion = item.seccion
                            };
                            valueResult.Result = entradaInfo;
                            valueResult.Estatus = true;
                        }
                        else
                        {
                            valueResult.Estatus = false;
                        }
                    }
                    informacion_entrada_Completed?.Invoke(this, new GenericEventArgs<ValueResult<RegistroEntrada>>(valueResult));
                }
            };
            service.obtener_entradaCompleted += (sender, e) =>
            {
                ValueResult<Model.obj_commit> result = new ValueResult<Model.obj_commit>();
                
                if (e.Result.ErrorNumber == "NO DATA")
                {
                    result.Estatus = true;
                    Model.obj_commit commit = new Model.obj_commit()
                    {
                        entrada = e.Result.entrada,
                        ErrorMessage = e.Result.ErrorMessage,
                        ErrorNumber = e.Result.ErrorNumber,
                    };
                    
                    result.Result = commit;
                }
                else
                    result.Message = e.Result.ErrorMessage;
                GenerarEntrada_Completed?.Invoke(this, new GenericEventArgs<ValueResult<Model.obj_commit>>(result));
              };
        }

        public event EventHandler<GenericEventArgs<ValueResult<List<BodegaInfo>>>> catalogo_bodegas_Completed;
        public event EventHandler<GenericEventArgs<ValueResult<List<ClienteInfo>>>> catalago_clientes_Completed;
        public event EventHandler<GenericEventArgs<ValueResult<RegistroEntrada>>> informacion_entrada_Completed;
        public event EventHandler<GenericEventArgs<ValueResult<Model.obj_commit>>> GuardarInfo_Completed;
        public event EventHandler<GenericEventArgs<ValueResult<Model.obj_commit>>> GenerarEntrada_Completed;

        public void GenerarEntrada()
        {
            service.obtener_entradaAsync();
        }

        public void GuardarInfo(RegistroEntrada entradaInfo)
        {
            service.abc_entradaAsync(entradaInfo.Entrada, entradaInfo.Cliente.id_cliente, entradaInfo.Usuario,
                                          entradaInfo.Bodega.idBodega, entradaInfo.Cantidad.ToString(),
                                          entradaInfo.Guia, entradaInfo.Ref, entradaInfo.Seccion);
        }

        public void ObtenerCatalagoBodegas()
        {
            service.catalogo_bodegasAsync();
        }

        public void ObtenerCatalagoClientes(string id_bodega)
        {
            service.catalogo_clientesAsync(id_bodega);
        }

        public void ObtenerInfoEntrada(string id_entrada)
        {
            service.informacion_entradaAsync(id_entrada);
        }
    }
}