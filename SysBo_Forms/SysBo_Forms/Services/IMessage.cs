﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysBo_Forms.Services
{
    public interface IMessage
    {
        void LongAlert(string message);
        void ShortAlert(string message);
    }
}
