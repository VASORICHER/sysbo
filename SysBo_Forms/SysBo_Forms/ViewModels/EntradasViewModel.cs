﻿using Android.Print;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using SysBo_Forms.Model;
using SysBo_Forms.Services;
using System;
using System.Collections.Generic;
using PdfSharp.Pdf;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SysBo_Forms.ViewModels
{
    public class EntradasViewModel : ViewModelBase, INavigationAware
    {
        RegistroEntrada registroEntrada;
        List<BodegaInfo> bodegas;
        List<ClienteInfo> clienteInfo;
        bool GeneradaLocal = false;
        public EntradasViewModel(INavigationService navigationService,
                                IPageDialogService pageDialog,
                                IErecap_Service _serviceErecap,
                                ISysbo_Service _serviceSysbo)
            : base(navigationService, pageDialog, _serviceErecap, _serviceSysbo)
        {
#if DEBUG
            Entrada = "E18104545";
#endif
            DescripcionBodega = "BODEGA";
            DescripcionCliente = "CLIENTE";
            Title = "REGISTRO ENTRADA";
            GuardarCommand = new DelegateCommand(Guardar);
            NuevoCommand = new DelegateCommand(Nuevo);
            BuscarEntrada = new DelegateCommand(BuscarInfoEntrada);
            ElegirBodega = new DelegateCommand(ShowElegirBodega);
            ElegirCliente = new DelegateCommand(ShowElegirCliente);
            LimpiarCommand = new DelegateCommand(LimpiarControles);
            EscanearCommand = new DelegateCommand(Escanear);

            _serviceSysbo.informacion_entrada_Completed += Service_Sysbo_informacion_entrada_Completed;
            _serviceSysbo.GuardarInfo_Completed += _serviceSysbo_GuardarInfo_Completed;
            _serviceSysbo.catalogo_bodegas_Completed += _serviceSysbo_catalogo_bodegas_Completed;
            _serviceSysbo.catalago_clientes_Completed += _serviceSysbo_catalago_clientes_Completed;
            _serviceSysbo.GenerarEntrada_Completed += _serviceSysbo_GenerarEntrada_Completed;
        }

        #region Metodos completed
        private void _serviceSysbo_GenerarEntrada_Completed(object sender, GenericEventArgs<ValueResult<obj_commit>> e)
        {
            if(e.Result.Estatus)
            {
                Entrada = e.Result.Result.entrada;
                string mensaje = "Entrada Creada";
                Xamarin.Forms.DependencyService.Get<IMessage>().LongAlert(mensaje);
                IsEnable = true;
            }
            IsBusy = false;
            GeneradaLocal = true;
        }

        private void _serviceSysbo_catalago_clientes_Completed(object sender, GenericEventArgs<ValueResult<List<ClienteInfo>>> e)
        {
            if (e.Result.Estatus)
                clienteInfo = new List<ClienteInfo>(e.Result.Result);
            else
                _pageDialog.DisplayAlertAsync("SYSBO", "Ocurrió un error al obtener catalago de clientes.","OK");
            IsBusy = false;
        }

        private void _serviceSysbo_catalogo_bodegas_Completed(object sender, GenericEventArgs<ValueResult<List<BodegaInfo>>> e)
        {
            IsBusy = false;
            if (e.Result.Estatus)
            {
                bodegas = new List<BodegaInfo>(e.Result.Result);
            }
            else
                _pageDialog.DisplayAlertAsync("SYSBO", "Ocurrío un error al obtener catalago de bodegas.", "OK");
        }
        #endregion


        #region Metodos completed del WebService
        private void _serviceSysbo_GuardarInfo_Completed(object sender, GenericEventArgs<ValueResult<obj_commit>> e)
        {
            if (e.Result.Estatus)
            {
                _pageDialog.DisplayAlertAsync("Sysbo", "Información guardada.", "OK");

                var printAttr = new PrintAttributes.Builder().SetMediaSize(PrintAttributes.MediaSize.IsoA6).Build();
                




                LimpiarControles();
            }
            else
            {
                _pageDialog.DisplayActionSheetAsync("Sysbo", $"Error: {e.Result.Message}","OK");
                BuscarInfoEntrada();
            }
        }

        private void Service_Sysbo_informacion_entrada_Completed(object sender, GenericEventArgs<ValueResult<RegistroEntrada>> e)
        {
            if(e.Result.Estatus)
            {
                var result = e.Result.Result;
                Entrada = result.Entrada;
                DescripcionBodega = result.Bodega.idBodega +"|"+ result.Bodega.Descripcion;
                DescripcionCliente = result.Cliente.id_cliente +"|"+ result.Cliente.razon_social;
                Cantidad = result.Cantidad;
                Guia = result.Guia;
                Ref = result.Ref;
                Seccion = result.Seccion;
                IsEnable = true;
            }
            else
            {
                _pageDialog.DisplayAlertAsync("SYSBO", "Entrada no existe en la Base de Datos.", "OK");
            }
            IsBusy = false;
        }
        #endregion


        #region Métodos locales
        private void ShowElegirBodega()
        {
            try
            {
                NavigationParameters parameter = new NavigationParameters();
                foreach (var infoBodega in bodegas)
                {
                    parameter.Add("bodegas", infoBodega.idBodega + "|" + infoBodega.Descripcion);
                }
                _navigationService.NavigateAsync("PopupBodega", parameter);
            }
            catch(Exception ex)
            {
                _pageDialog.DisplayAlertAsync("SYSBO", "Error: "+ex.Message,"OK");
            }
        }

        private void ShowElegirCliente()
        {
            try
            {
                if (DescripcionBodega != "BODEGA")
                {
                    var parameter = new NavigationParameters();
                    foreach (var clienteInformacion in clienteInfo)
                    {
                        parameter.Add("clientes", clienteInformacion.id_cliente + "|" + clienteInformacion.razon_social);
                    }
                    _navigationService.NavigateAsync("PopupCliente", parameter);
                }
                else
                {
                    _pageDialog.DisplayAlertAsync("SYSBO", "Primero seleccione la bodega", "OK");
                }
            }
            catch(Exception ex)
            {
                _pageDialog.DisplayAlertAsync("SYSBO", "Error: " + ex.Message, "OK");
            }
        }

        private void BuscarInfoEntrada()
        {
            if (!string.IsNullOrEmpty(Entrada) && !GeneradaLocal)
            {
                IsBusy = true;
                service_Sysbo.ObtenerInfoEntrada(Entrada.Trim());
            }
            else
            {
                _pageDialog.DisplayAlertAsync("SYSBO", "No hay información de la entrada.", "OK");
            }
        }

        private async void Nuevo()
        {
            //Mandar a llamar metodo para traer entrada
            var response = await _pageDialog.DisplayAlertAsync("SYSBO", "¿Crear entrada nueva?", "Si", "Cancelar");
            if (response)
            {
                IsBusy = true;
                LimpiarControles();
                service_Sysbo.GenerarEntrada();
            }
        }

        private async void Guardar()
        {
            //guardar la información en la ventana
            if ((!string.IsNullOrEmpty(DescripcionBodega) && DescripcionBodega != "Bodega") && (!string.IsNullOrEmpty(DescripcionCliente) && DescripcionCliente != "Cliente"))
            {
                string[] bodegaArray = DescripcionBodega.Split('|');
                Bodega = new BodegaInfo()
                {
                    idBodega = bodegaArray[0],
                    Descripcion = bodegaArray[1]
                };

                string[] clienteArray = DescripcionCliente.Split('|');
                Cliente = new ClienteInfo()
                {
                    id_cliente = clienteArray[0],
                    razon_social = clienteArray[1]
                };
            }
            else
            {
                await _pageDialog.DisplayAlertAsync("SYSBO", "Campos bodega y cliente no deben estar vacíos.", "OK");
                return;
            }
            registroEntrada = new RegistroEntrada()
            {
                Entrada = Entrada,
                Bodega = Bodega,
                Cliente = Cliente,
                Cantidad = Cantidad,
                Guia = Guia,
                Ref = Ref,
                Seccion = Seccion,
                Usuario = Usuario
            };
            int result = validaciones(registroEntrada);

            if (result == 1)
            {
                await _pageDialog.DisplayAlertAsync("SYSBO", "Entrada vacía, ingrese o genere una nueva.", "OK");
                return;
            }
            else if (result == 2)
            {
                await _pageDialog.DisplayAlertAsync("SYSBO", "Seleccione un cliente.", "OK");
                return;
            }
            else if (result == 3)
            {
                await _pageDialog.DisplayAlertAsync("SYSBO", "Seleccione bodega.", "OK");
                return;
            }
            else if (result == 4)
            {
                var respuesta = await _pageDialog.DisplayAlertAsync("SYSBO", "Cantidad vacía, ¿Desea continuar?", "Si", "No");
                if (!respuesta)
                {
                    IsBusy = false;
                    return;
                }
            }
            else
            {
                IsBusy = true;
                service_Sysbo.GuardarInfo(registroEntrada);
            }

            IsBusy = false;
        }

        static int validaciones(RegistroEntrada registroEntrada)
        {
            int RESULT = 0;
            if (string.IsNullOrEmpty(registroEntrada.Entrada))
            {
                RESULT = 1;
                return RESULT;
            }
            if (registroEntrada.Cliente == null)
            {
                RESULT = 2;
                return RESULT;
            }
            if (registroEntrada.Bodega == null)
            {
                RESULT = 3;
                return RESULT;
            }
            if (registroEntrada.Cantidad <= 0)
            {
                RESULT = 4;
                return RESULT;
            }

            return RESULT;
        }

        void LimpiarControles()
        {
            Entrada = string.Empty;
            DescripcionBodega = "BODEGA";
            DescripcionCliente = "CLIENTE";
            Cantidad = 0;
            Guia = string.Empty;
            Ref = string.Empty;
            Seccion = string.Empty;
            GeneradaLocal = false;
            IsEnable = false;
        }

        void CargarClientes(string bodegaInfo)
        {
            string[] arregloBodega = bodegaInfo.Split('|');
            service_Sysbo.ObtenerCatalagoClientes(arregloBodega[0]);
        }

        void Escanear()
        {
            _navigationService.NavigateAsync("scaner");
        }
        #endregion


        #region Metodos de navegacion Prism
        //Cuando se va pasa por aqui
        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            //Menu
        }

        //Primero pasa por aqui
        //Pasa por aqui cuando regresa la pagina
        public void OnNavigatedTo(INavigationParameters parameters)
        {
            service_Sysbo.informacion_entrada_Completed += Service_Sysbo_informacion_entrada_Completed;

            if (parameters.ContainsKey("bodega"))
            {
                IsBusy = true;
                DescripcionBodega = parameters["bodega"].ToString();
                DescripcionCliente = "Cliente";
                CargarClientes(DescripcionBodega);
            }
            else if (parameters.ContainsKey("cliente"))
                DescripcionCliente = parameters["cliente"].ToString();
            else if(parameters.ContainsKey("entrada"))
            {
                IsBusy = true;
                Entrada = parameters["entrada"].ToString();
                if(Entrada.Length>9)
                    Entrada = Entrada.Substring(0, 9);
                service_Sysbo.ObtenerInfoEntrada(Entrada.Trim());
            }
            else
            {
                IsBusy = true;
                service_Sysbo.ObtenerCatalagoBodegas();
            }
        }
        #endregion


        #region Propiedades

        private BodegaInfo _Bodega;

        public BodegaInfo Bodega
        {
            get => _Bodega;
            set => SetProperty(ref _Bodega, value);
        }

        private ClienteInfo _Cliente;

        public ClienteInfo Cliente
        {
            get => _Cliente;
            set => SetProperty(ref _Cliente, value);
        }

        private int _Cantidad;

        public int Cantidad
        {
            get => _Cantidad;
            set => SetProperty(ref _Cantidad, value);
        }

        private string _Guia;

        public string Guia
        {
            get => _Guia;
            set => SetProperty(ref _Guia, value);
        }

        private string _Ref;

        public string Ref
        {
            get => _Ref;
            set => SetProperty(ref _Ref, value);
        }

        private string _Seccion;

        public string Seccion
        {
            get => _Seccion;
            set => SetProperty(ref _Seccion, value);
        }
        
        private string _DescripcionBodega;

        public string DescripcionBodega
        {
            get => _DescripcionBodega;
            set => SetProperty(ref _DescripcionBodega, value);
        }

        private string _DescripcionCliente;

        public string DescripcionCliente
        {
            get => _DescripcionCliente;
            set => SetProperty(ref _DescripcionCliente, value);
        }

        private bool _IsEnable;
        public bool IsEnable
        {
            get => _IsEnable;
            set => SetProperty(ref _IsEnable, value);
        }
        #endregion



        public DelegateCommand GuardarCommand { get; private set; }
        public DelegateCommand NuevoCommand { get; private set; }
        public DelegateCommand BuscarEntrada { get; private set; }
        public DelegateCommand ElegirBodega { get; private set; }
        public DelegateCommand ElegirCliente { get; private set; }
        public DelegateCommand LimpiarCommand { get; private set; }
        public DelegateCommand EscanearCommand { get; private set; }
    }
}
