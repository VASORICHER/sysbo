﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SysBo_Forms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AccesoView : ContentPage
    {
        public AccesoView()
        {
            InitializeComponent();
            entryUsuario.Completed += (object sender, EventArgs e) => entryClave.Focus();
            entryClave.Completed += (object sender, EventArgs e) => buttonEntrar.Focus();
        }
    }
}