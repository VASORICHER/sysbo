﻿using SysBo_Forms.Services;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Prism.Common;

namespace SysBo_Forms.ViewModels
{
    public class MenuViewModel : ViewModelBase, INavigationAware
    {
        public MenuViewModel(INavigationService navigationService,
                             IPageDialogService pageDialog,
                             IErecap_Service _serviceErecap,
                             ISysbo_Service _serviceSysbo)
            :base(navigationService,pageDialog,_serviceErecap, _serviceSysbo)
        {
            NavegarCommand = new DelegateCommand<string>(Navegar);
        }
        async void Navegar(string page)
        {
            IsBusy = true;
            await _navigationService.NavigateAsync(page,null,true);
            IsBusy = false;
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            
        }

        public DelegateCommand<string> NavegarCommand { get; private set; }
    }
}
