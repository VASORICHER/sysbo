﻿using System;
using System.IO;
using System.Threading.Tasks;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Prism.Services;

namespace SysBo_Forms.Media
{
    public class XamMedia
    {
        IPageDialogService _pageDialog;
        string Entrada = string.Empty;
        public XamMedia(IPageDialogService pageDialog, string _entrada)
        {
            _pageDialog = pageDialog;            
        }
        public async Task<byte[]> GetByteFromGallery()
        {
            if(!CrossMedia.Current.IsPickPhotoSupported)
            {
                await _pageDialog.DisplayAlertAsync("Sin galería", ":( Galería no disponible.", "OK");
                return null;
            }

            var mediaFile = await CrossMedia.Current.PickPhotoAsync();

            var photo = mediaFile;
            if (photo == null)
                return null;

            MediaFile myImage = photo;
            byte[] myByteArray;
            using (var stream = new MemoryStream())
            {
                myImage.GetStream().CopyTo(stream);
                myImage.Dispose();
                myByteArray = stream.ToArray();
            }

            return myByteArray;
        }

        public async Task<byte[]> GetBytesFromCamera()
        {
            if(!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await _pageDialog.DisplayAlertAsync("No Camera", ":( No camera available.", "OK");
                return null;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                Directory = $"ERECAP_{Entrada}",
                Name = Guid.NewGuid().ToString().Replace("-", ""),
                SaveToAlbum = true
            });

            if (file == null)
                return null;

            MediaFile myImage = file;
            byte[] myByteArray;
            using (var stream = new MemoryStream())
            {
                myImage.GetStream().CopyTo(stream);
                myImage.Dispose();
                myByteArray = stream.ToArray();
            }

            return myByteArray;
        }
    }
}
