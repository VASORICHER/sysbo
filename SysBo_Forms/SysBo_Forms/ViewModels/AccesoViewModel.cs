﻿using System;
using SysBo_Forms.Services;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace SysBo_Forms.ViewModels
{
    public class AccesoViewModel : ViewModelBase, INavigationAware
    {
        //Primero pasa por aqui
        public void OnNavigatingTo(NavigationParameters parameters)
        {
            App.Current.Properties.Clear();

        }
        public AccesoViewModel(INavigationService navigationService,
                               IPageDialogService pageDialog,
                               IErecap_Service _serviceErecap,
                               ISysbo_Service _serviceSysbo)
            : base(navigationService, pageDialog, _serviceErecap, _serviceSysbo)
        {

            EntrarCommand = new DelegateCommand(Entrar);

            service_Erecap.ValidarUsuario_Completed += async (s, a) =>
            {
                if (a.Result.Estatus)
                {
                    var limite = a.Result.Result.LimiteFotos;
                    LimiteEntrada = limite.CountEntrada;
                    LimiteRevision = limite.CountRevision;
                    LimiteEmbarque = limite.CountEmbarque;
                    LimiteSalida = limite.CountSalida;

                    await _navigationService.NavigateAsync("/Menu/NavigationPage/Base");// (new Uri("Menu",UriKind.Absolute));
                }
                else
                    await pageDialog.DisplayAlertAsync("Información de acceso", a.Result.Message, "OK");
                IsBusy = false;
            };

            Title = "Acceso";
        }


        private void Entrar()
        {
            IsBusy = true;
            service_Erecap.ValidarUsuario(Usuario, Clave);
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
#if DEBUG
            Usuario = "EDRO";
            Clave = "0607";
#else
            Usuario = string.Empty;
            Clave = string.Empty;
            App.Current.Properties.Clear();
#endif
        }

        public DelegateCommand EntrarCommand { get; private set; }
    }
}
