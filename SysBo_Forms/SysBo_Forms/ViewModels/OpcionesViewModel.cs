﻿using SysBo_Forms.Model;
using SysBo_Forms.Services;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace SysBo_Forms.ViewModels
{
    public class OpcionesViewModel : ViewModelBase, INavigatedAware
    {
        public OpcionesViewModel(INavigationService navigationService,
                                IPageDialogService pageDialog,
                                IErecap_Service _serviceErecap,
                                ISysbo_Service _serviceSysbo)
            :base(navigationService,pageDialog,_serviceErecap, _serviceSysbo)
        {
            SelectCommand = new DelegateCommand<string>(Navegar);

            service_Erecap.ObtenerCountFotos_Completed -= Service_ObtenerCountFotos_Completed;
            service_Erecap.ObtenerCountFotos_Completed += Service_ObtenerCountFotos_Completed;

            IsBusy = true;
            Title = $"Opciones | {Entrada}";
        }

        private void Service_ObtenerCountFotos_Completed(object sender, GenericEventArgs<ValueResult<CountFotos>> e)
        {
            if(e.Result.Estatus)
            {
                var fotos = e.Result.Result;
                CountEntrada = fotos.CountEntrada;
                CountRevision = fotos.CountRevision;
                CountEmbarque = fotos.CountEmbarque;
                CountSalida = fotos.CountSalida;
                Cliente = fotos.Cliente;
                CountBultos = fotos.Bultos;
            }
        }

        private async void Navegar(string _pagina)
        {
            var count = 0;
            if (_pagina == "Entrada")
            {
                Tipo = "K";
                count = CountEntrada;
            }
            else if(_pagina == "Embarque")
            {
                Tipo = "B";
                count = CountEmbarque;
            }
            else if (_pagina == "Salida")
            {
                Tipo = "S";
                count = CountSalida;
            }
            else
            {
                Tipo = "R";
                count = CountRevision;
            }

            var navigationParams = new NavigationParameters
            {
                {
                    "count",count
                }
            };
            await _navigationService.NavigateAsync("Galeria", navigationParams);
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("fotos"))
            {
                CountFotos fotos = (CountFotos)parameters["fotos"];
                CountEntrada = fotos.CountEntrada;
                CountRevision = fotos.CountRevision;
                CountEmbarque = fotos.CountEmbarque;
                CountSalida = fotos.CountSalida;
                Cliente = fotos.Cliente;
                CountBultos = fotos.Bultos;
            }
            else
                service_Erecap.ObtenerCountFotos(Entrada);
        }
        private int _CounEntrada;
        public int CountEntrada
        {
            get => _CounEntrada;
            set => SetProperty(ref _CounEntrada, value);
        }
        private int _CountRevision;
        public int CountRevision
        {
            get => _CountRevision;
            set => SetProperty(ref _CountRevision, value);
        }
        private int _CountEmbarque;
        public int CountEmbarque
        {
            get => _CountEmbarque;
            set => SetProperty(ref _CountEmbarque, value);
        }
        private int _CountSalida;
        public int CountSalida
        {
            get => _CountSalida;
            set => SetProperty(ref _CountSalida, value);
        }

        private string _Cliente;

        public string Cliente
        {
            get => _Cliente;
            set => SetProperty(ref _Cliente, value);
        }

        private decimal _CountBultos;
        public decimal CountBultos
        {
            get => _CountBultos;
            set => SetProperty(ref _CountBultos, value);
        }
        public DelegateCommand<string> SelectCommand { get; private set; }
    }
}
