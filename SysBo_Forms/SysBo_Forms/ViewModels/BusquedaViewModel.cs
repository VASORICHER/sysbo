﻿using SysBo_Forms.Services;
using Prism.AppModel;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System;

namespace SysBo_Forms.ViewModels
{
    public class BusquedaViewModel : ViewModelBase, INavigationAware
    {
        public BusquedaViewModel(INavigationService navigationService, IPageDialogService pageDialog, IErecap_Service _serviceErecap, ISysbo_Service _serviceSysbo)
            :base(navigationService,pageDialog,_serviceErecap, _serviceSysbo)
        {
            BuscarCommand = new DelegateCommand(Buscar);
            EscanearCommand = new DelegateCommand(scan);

#if DEBUG
            Entrada = "E15112483";
#endif
            Title = "Busqueda";
        }

        private void Buscar()
        {
            IsBusy = true;
            service_Erecap.ObtenerCountFotos(Entrada);
        }

        private void scan()
        {
            _navigationService.NavigateAsync("scaner");
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            service_Erecap.ObtenerCountFotos_Completed -= Service_ObtenerCountFotos_Completed;
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("entrada"))
            {
                IsBusy = true;
                Entrada = parameters["entrada"].ToString();
                Entrada = Entrada.Substring(0, 9);
                service_Erecap.ObtenerCountFotos(Entrada);
            }
            service_Erecap.ObtenerCountFotos_Completed -= Service_ObtenerCountFotos_Completed;
            service_Erecap.ObtenerCountFotos_Completed += Service_ObtenerCountFotos_Completed;
        }

        private void Service_ObtenerCountFotos_Completed(object sender, GenericEventArgs<Model.ValueResult<Model.CountFotos>> e)
        {
            if (e.Result.Estatus)
            {
                var counts = e.Result.Result;
                var navigationParams = new NavigationParameters
                {
                    {"fotos",counts }
                };
                _navigationService.NavigateAsync("Opciones", navigationParams);
            }
            else
                _pageDialog.DisplayAlertAsync("Información de busqueda", e.Result.Message, "OK");
            IsBusy = false;
        }

        public DelegateCommand BuscarCommand { get; private set; }
        public DelegateCommand EscanearCommand { get; private set; }
    }
}
