﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Plugin.CurrentActivity;
using Plugin.Permissions;
using Prism;
using Prism.Ioc;
using SysBo_Forms.Services;
using System;

namespace SysBo_Forms.Droid
{
    [Activity(Label = "SYSBO", Icon = "@mipmap/sysbo", Theme = "@style/MainTheme",
            MainLauncher = true,
            ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            try
            {
                TabLayoutResource = Resource.Layout.Tabbar;
                ToolbarResource = Resource.Layout.Toolbar;

                base.OnCreate(bundle);
                ZXing.Net.Mobile.Forms.Android.Platform.Init();
                global::Xamarin.Forms.Forms.Init(this, bundle);
                Rg.Plugins.Popup.Popup.Init(this, bundle);
                CrossCurrentActivity.Current.Activity = this;
                LoadApplication(new App(new AndroidInitializer()));
            }
            catch(Exception ex)
            {
                var message = ex.Message;
            }
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            global::ZXing.Net.Mobile.Android.PermissionsHandler.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        static Erecap_Service_Android DroidService = new Erecap_Service_Android();
        static Sysbo_Service_Android DroidSysboService = new Sysbo_Service_Android();

        public class AndroidInitializer : IPlatformInitializer
        {
            public void RegisterTypes(IContainerRegistry containerRegistry)
            {
                containerRegistry.RegisterInstance<IErecap_Service>(DroidService);
                containerRegistry.RegisterInstance<ISysbo_Service>(DroidSysboService);
            }
        }
    }
}

