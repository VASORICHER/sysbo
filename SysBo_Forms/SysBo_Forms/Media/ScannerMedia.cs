﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Navigation;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace SysBo_Forms.Media
{
    public class ScannerMedia : ZXingScannerPage, INavigatedAware
    {
        string entrada = "";

        public ScannerMedia()
        {
            this.OnScanResult += (result) =>
              {
                  IsScanning = false;
                  Device.BeginInvokeOnMainThread(() =>
                  {
                      Navigation.PopAsync();
                      entrada = result.Text;
                  });
              };
            var nav = new NavigationParameters
            {
                {"entrada",entrada }
            };
        }
        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            if (entrada != "")
                parameters.Add("entrada", entrada);
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }
    }
}
