﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysBo_Forms.Model
{
    public class GeneralInfo
    {
        public string IdUsuario { get; set; }
        public string Nombre { get; set; }
        public CountFotos LimiteFotos { get; set; }
    }
}
