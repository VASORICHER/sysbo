﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysBo_Forms.Model
{
    public class obj_commit
    {
        public string entrada { get; set; }
        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
        public string obs { get; set; }

        public obj_commit()
        {
            entrada = "NO DATA";
            ErrorNumber = "NO DATA";
            ErrorMessage = "NO DATA";
            obs = "NO DATA";
        }
    }
}
