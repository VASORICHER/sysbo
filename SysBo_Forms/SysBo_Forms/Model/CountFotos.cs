﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysBo_Forms.Model
{
    public class CountFotos
    {
        public int CountEntrada { get; set; }
        public int CountRevision { get; set; }
        public int CountEmbarque { get; set; }
        public int CountSalida { get; set; }
        public string Cliente { get; set; }
        public decimal Bultos { get; set; }
    }
}
