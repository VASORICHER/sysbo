﻿using System;
using System.Collections.Generic;
using System.Linq;
using SysBo_Forms.iOS;
using SysBo_Forms.iOS.ERECAP_WS;
using SysBo_Forms.Model;
using SysBo_Forms.Services;
using Xamarin.Forms;

namespace SysBo_Forms.iOS
{
    class erecap_Service_iOS : IErecap_Service
    {
        readonly WebService service;
        public erecap_Service_iOS()
        {
            service.ValidarUsuarioCompleted += Service_ValidarUsuarioCompleted;
            service.ObtenerFotosCompleted += Service_ObtenerFotosCompleted;
            service.SubirFotosCompleted += Service_SubirFotosCompleted;

        }

        private void Service_SubirFotosCompleted(object sender, SubirFotosCompletedEventArgs e)
        {
            ValueResult<Model.FotosInfo> result = new ValueResult<Model.FotosInfo>();
            if (e.Result.Estatus)
            {
                var fotos = new Model.FotosInfo();
                var existentes = e.Result.Result.FotosExistentes;
                fotos.FotosExistentes = new Model.CountFotos()
                {
                    CountEntrada = existentes.CountEntrada,
                    CountRevision = existentes.CountRevision,
                    CountEmbarque = existentes.CountEmbarque,
                    CountSalida = existentes.CountSalida
                };

                fotos.ListaFotosBytes = e.Result.Result.ListaFotosBytes ?? fotos.ListaFotosBytes;
                result.Result = fotos;
                result.Estatus = true;
            }
            else
                result.Message = e.Result.Message;
            SubirFotos_Completed?.Invoke(this, new GenericEventArgs<ValueResult<Model.FotosInfo>>(result));
        }

        private void Service_ObtenerFotosCompleted(object sender, ObtenerFotosCompletedEventArgs e)
        {
            ValueResult<Model.CountFotos> resultCount = new ValueResult<Model.CountFotos>();
            ValueResult<IList<byte[]>> resultFoto = new ValueResult<IList<byte[]>>();
            if (e.Result.Estatus)
            {
                var result = e.Result.Result;
                if (isCount)
                {
                    var foto = new Model.CountFotos()
                    {
                        CountEntrada = result.FotosExistentes.CountEntrada,
                        CountRevision = result.FotosExistentes.CountRevision,
                        CountEmbarque = result.FotosExistentes.CountEmbarque,
                        CountSalida = result.FotosExistentes.CountSalida
                    };
                    resultCount.Estatus = true;
                    resultCount.Result = foto;
                    ObtenerCountFotos_Completed?.Invoke(this, new GenericEventArgs<ValueResult<Model.CountFotos>>(resultCount));
                }
                else
                {
                    var foto = result.ListaFotosBytes?.ToList() ?? new List<byte[]>();
                    resultFoto.Result = foto;
                    resultFoto.Estatus = true;
                    ObtenerFotos_Completed?.Invoke(this, new GenericEventArgs<ValueResult<IList<byte[]>>>(resultFoto));
                }
            }
        }

        private void Service_ValidarUsuarioCompleted(object sender, ValidarUsuarioCompletedEventArgs e)
        {
            ValueResult<Model.GeneralInfo> result = new ValueResult<Model.GeneralInfo>();

            if (e.Result.Estatus)
            {
                var general = new Model.GeneralInfo()
                {
                    IdUsuario = e.Result.Result.IdUsuario,
                    Nombre = e.Result.Result.Nombre,
                    LimiteFotos = new Model.CountFotos()
                    {
                        CountEntrada = e.Result.Result.LimiteFotos.CountEntrada,
                        CountRevision = e.Result.Result.LimiteFotos.CountRevision,
                        CountEmbarque = e.Result.Result.LimiteFotos.CountEmbarque,
                        CountSalida = e.Result.Result.LimiteFotos.CountSalida
                    }
                };
                result.Result = general;
                result.Estatus = true;
            }
            else
                result.Message = e.Result.Message;
            ValidarUsuario_Completed?.Invoke(this, new GenericEventArgs<ValueResult<Model.GeneralInfo>>(result));
        }

        public event EventHandler<GenericEventArgs<ValueResult<Model.GeneralInfo>>> ValidarUsuario_Completed;
        public event EventHandler<GenericEventArgs<ValueResult<IList<byte[]>>>> ObtenerFotos_Completed;
        public event EventHandler<GenericEventArgs<ValueResult<Model.CountFotos>>> ObtenerCountFotos_Completed;
        public event EventHandler<GenericEventArgs<ValueResult<Model.FotosInfo>>> SubirFotos_Completed;

        bool isCount = false;
        public void ObtenerCountFotos(string _entrada)
        {
            isCount = true;
            service.ObtenerFotosAsync(_entrada, null);
        }

        public void ObtenerFotos(string _entrada, string _tipo)
        {
            isCount = false;
            service.ObtenerFotosAsync(_entrada, _tipo);
        }

        public void SubirFotos(string _entrada, string _tipo, byte[] _imageBytes, string _usuario)
        {
            service.SubirFotosAsync(_entrada, _tipo, _imageBytes, _usuario);
        }

        public void ValidarUsuario(string _usuario, string _clave)
        {
            service.ValidarUsuarioAsync(_usuario, _clave);
        }


    }
}