﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using SysBo_Forms.Model;
using SysBo_Forms.Services;

namespace SysBo_Forms.ViewModels
{
    public class ClientePopupViewModel : ViewModelBase, INavigationAware
    {
        public ClientePopupViewModel(INavigationService navigationService, IPageDialogService pageDialog, IErecap_Service _serviceErecap, ISysbo_Service _serviceSysbo)
            : base(navigationService, pageDialog, _serviceErecap, _serviceSysbo)
        {
            CerrarCommand = new DelegateCommand(Cerrar);
            SearchCommand = new DelegateCommand(BuscarTexto);
            _serviceSysbo.catalago_clientes_Completed += _serviceSysbo_catalago_clientes_Completed;
        }

        private void BuscarTexto()
        {
            SearchText = SearchText.ToUpper();
            if (string.IsNullOrEmpty(SearchText))
                ListaClientes = _ClientesUnFiltered;
            else
            {
                _ClientesFiltered = new ObservableCollection<ClienteInfo>(_ClientesUnFiltered.Where(i => (i is ClienteInfo && (((ClienteInfo)i).razon_social.ToUpper().Contains(SearchText)))));
                ListaClientes = _ClientesFiltered;
            }
        }
        private void Cerrar()
        {
            _navigationService.GoBackAsync();
        }

        private void _serviceSysbo_catalago_clientes_Completed(object sender, GenericEventArgs<ValueResult<List<ClienteInfo>>> e)
        {
            if(e.Result.Estatus)
            {
                var lista = new ObservableCollection<ClienteInfo>();
                foreach (var cliente in e.Result.Result)
                {
                    lista.Add(cliente);
                }
                ListaClientes = new ObservableCollection<ClienteInfo>(lista);
            }
            IsBusy = false;
        }

        //Al salir pasa por aqui
        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            if(ClienteSelected != null)
                parameters.Add("cliente", ClienteSelected.id_cliente);
        }

        //Al entrar pasa por aqui
        public void OnNavigatedTo(INavigationParameters parameters)
        {
            if(parameters.ContainsKey("clientes"))
            {
                IsBusy = true;
                CargarClientes(parameters);
            }
        }

        void CargarClientes(INavigationParameters parameters)
        {
            try
            {
                var ls = new ObservableCollection<ClienteInfo>();
                foreach (var clienteinfo in parameters)
                {
                    string[] arregloCliente = clienteinfo.Value.ToString().Split('|');
                    var cliente = new ClienteInfo()
                    {
                        id_cliente = arregloCliente[0],
                        razon_social = arregloCliente[1],
                        FullName = arregloCliente[0]+ " | " + arregloCliente[1]
                    };
                    ls.Add(cliente);
                }
                ListaClientes = new ObservableCollection<ClienteInfo>(ls);
                _ClientesUnFiltered = new ObservableCollection<ClienteInfo>(ls);
                IsBusy = false;
            }
            catch(Exception ex)
            {
                _pageDialog.DisplayAlertAsync("SYSBO", "Error: " + ex.Message, "OK");
            }
        }

        private ObservableCollection<ClienteInfo> _ListaClientes;
        private ObservableCollection<ClienteInfo> _ClientesFiltered;
        private ObservableCollection<ClienteInfo> _ClientesUnFiltered;

        public ObservableCollection<ClienteInfo> ListaClientes
        {
            get
            {
                if (_ListaClientes == null)
                    _ListaClientes = new ObservableCollection<ClienteInfo>();
                return _ListaClientes;
            }
            set => SetProperty(ref _ListaClientes, value);
        }

        private ClienteInfo _ClienteSelected;

        public ClienteInfo ClienteSelected
        {
            get => _ClienteSelected;
            set
            {
                if (_ClienteSelected != value)
                {
                    _ClienteSelected = value;
                    string cl = ClienteSelected.id_cliente + "|" + ClienteSelected.razon_social;
                    var navPar = new NavigationParameters();
                    navPar.Add("cliente", cl);
                    _navigationService.GoBackAsync(navPar);
                }
            }
        }

        private string _SearchText;
        public string SearchText
        {
            get => _SearchText;
            set => SetProperty(ref _SearchText, value);
        }

        public DelegateCommand CerrarCommand { get; private set; }
        public DelegateCommand SearchCommand { get; private set; }
    }
}
