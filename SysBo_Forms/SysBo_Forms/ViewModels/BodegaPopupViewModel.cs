﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using SysBo_Forms.Model;
using SysBo_Forms.Services;

namespace SysBo_Forms.ViewModels
{
    public class BodegaPopupViewModel: ViewModelBase, INavigationAware
    {
        public BodegaPopupViewModel(INavigationService navigationService, IPageDialogService pageDialog, IErecap_Service _serviceErecap, ISysbo_Service _serviceSysbo)
            : base(navigationService, pageDialog, _serviceErecap, _serviceSysbo)
        {
            _serviceSysbo.catalogo_bodegas_Completed += _serviceSysbo_catalogo_bodegas_Completed;
            CerrarCommand = new DelegateCommand(Cerrar);
        }
        private void Cerrar()
        {
            _navigationService.GoBackAsync();
        }
        private void _serviceSysbo_catalogo_bodegas_Completed(object sender, GenericEventArgs<ValueResult<List<BodegaInfo>>> e)
        {
            if (e.Result.Estatus)
            {
                var lista = new List<BodegaInfo>();
                foreach (var bodega in e.Result.Result)
                {
                    if (bodega != null)
                    {
                        lista.Add(bodega);
                    }
                }
                ListaBodegas = new List<BodegaInfo>(lista);
            }
            IsBusy = false;
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            if (BodegaSelected != null)
                parameters.Add("bodega", BodegaSelected.idBodega);
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            IsBusy = true;
            if (parameters.ContainsKey("bodegas"))
                CargarBodegas(parameters);
            IsBusy = false;
        }

        private List<BodegaInfo> _ListaBodegas;

        public List<BodegaInfo> ListaBodegas
        {
            get
            {
                if (_ListaBodegas == null)
                    _ListaBodegas = new List<BodegaInfo>();
                return _ListaBodegas;
            }
            set => SetProperty(ref _ListaBodegas, value);
        }

        void CargarBodegas(INavigationParameters parameters)
        {
            var ls = new List<BodegaInfo>();
            foreach (var item in parameters)
            {
                string[] arregloBodegas = item.Value.ToString().Split('|');
                var bodega = new BodegaInfo()
                {
                    idBodega = arregloBodegas[0],
                    Descripcion = arregloBodegas[1]
                };
                ls.Add(bodega);
            }
            ListaBodegas = new List<BodegaInfo>(ls);
        }

        private BodegaInfo _BodegaSelected;

        public BodegaInfo BodegaSelected
        {
            get => _BodegaSelected;
            set
            {
                if (_BodegaSelected != value)
                {
                    _BodegaSelected = value;
                    string wh = BodegaSelected.idBodega + "|" + BodegaSelected.Descripcion;
                    var navPar = new NavigationParameters();
                    navPar.Add("bodega", wh);
                    _navigationService.GoBackAsync(navPar);
                }
            }
        }

        public DelegateCommand CerrarCommand { get; private set; }
    }
}
