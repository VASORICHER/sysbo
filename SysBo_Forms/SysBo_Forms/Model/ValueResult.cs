﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysBo_Forms.Model
{
    public class ValueResult<T>
    {
        public bool Estatus { get; set; }
        public T Result { get; set; }
        public string Message { get; set; }
    }
}
