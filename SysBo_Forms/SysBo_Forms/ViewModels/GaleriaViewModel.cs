﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using SysBo_Forms.Media;
using SysBo_Forms.Services;
using Prism.Commands;
using Prism.Services;
using Xamarin.Forms;
using Prism.Navigation;

namespace SysBo_Forms.ViewModels
{
    public class GaleriaViewModel : ViewModelBase, INavigationAware
    {
        XamMedia xamMedia;

        public GaleriaViewModel(INavigationService navigationService,
                                IPageDialogService pageDialog,
                                IErecap_Service _serviceErecap,
                                ISysbo_Service _serviceSysbo)
            :base(navigationService,pageDialog,_serviceErecap, _serviceSysbo)
        {
            SubirCommand = new DelegateCommand(Subir);
            ActualizarListaCommand = new DelegateCommand(Actualizar);
            OpenGalleryCommand = new DelegateCommand(OpenGallery);

            service_Erecap.ObtenerFotos_Completed += Service_ObtenerFotos_Completed;
            service_Erecap.SubirFotos_Completed += Service_SubirFotos_Completed;

            xamMedia = new XamMedia(_pageDialog, Entrada);

            string TipoText = string.Empty;
            if (Tipo == "K") TipoText = "Entrada";
            else if (Tipo == "B") TipoText = "Embarue";
            else if (Tipo == "R") TipoText = "Revision";
            else if (Tipo == "S") TipoText = "Salida";

            Title = $"{Entrada}";
        }

        private void UpdateViewControls(int _count)
        {
            if(_count > 0)
            {
                HayFotos = true;
                NoHayFotos = false;
            }
            else
            {
                HayFotos = false;
                NoHayFotos = true;
            }
        }

        private async void OpenGallery()
        {
            if (PermiteMasFotos())
            {
                byte[] ByteArray = await xamMedia.GetByteFromGallery();
                SubirByteArray(ByteArray);
            }
            else
                await _pageDialog.DisplayAlertAsync("Limite de fotos", $"Ya se cumplió el límite de fotos", "OK");
        }

        private void Service_SubirFotos_Completed(object sender, GenericEventArgs<Model.ValueResult<Model.FotosInfo>> e)
        {
            var lista = new List<ImageSource>();
            foreach (var item in e.Result.Result.ListaFotosBytes)
            {
                ImageSource retSource = null;
                if (item != null)
                    retSource = ImageSource.FromStream(() => new MemoryStream(item));
                lista.Add(retSource);
            }
            ListaImageSource = new ObservableCollection<ImageSource>(lista);
            IsBusy = false;
            var count = lista.Count;
            UpdateViewControls(count);
        }

        private void Service_ObtenerFotos_Completed(object sender, GenericEventArgs<Model.ValueResult<IList<byte[]>>> e)
        {
            if (e.Result.Estatus)
            {
                var lista = new List<ImageSource>();
                foreach (var item in e.Result.Result)
                {
                    ImageSource retSource = null;
                    if (item != null)
                        retSource = ImageSource.FromStream(() => new MemoryStream(item));
                    lista.Add(retSource);
                }
                ListaImageSource = new ObservableCollection<ImageSource>(lista);
                var count = lista.Count;
                UpdateViewControls(count);
            }
            else
                _pageDialog.DisplayAlertAsync("Descarga de imagenes", e.Result.Message, "OK");
            IsBusy = false;
        }

        

        private void Actualizar()
        {
            IsBusy = true;
            service_Erecap.ObtenerFotos(Entrada, Tipo);
        }

        private async void Subir()
        {
            if(PermiteMasFotos())
            {
                IsBusy = true;
                byte[] ByteArray = await xamMedia.GetBytesFromCamera();
                SubirByteArray(ByteArray);
            }
        }

        private async void SubirByteArray(byte[] _byteArray)
        {
            float sizeInMB = (_byteArray.Length / 1024f) / 1024f;
            if (sizeInMB <= 1)
                service_Erecap.SubirFotos(Entrada, Tipo, _byteArray, Usuario);
            else
            {
                await _pageDialog.DisplayAlertAsync("Tamaño de foto", $"El tamaño es mayor a 1Mb favor de configurar la camara.", "OK");
                IsBusy = false;
            }                
        }

        bool PermiteMasFotos()
        {
            int count = 0;
            count = Tipo == "K" ? LimiteEntrada : Tipo == "B" ? LimiteEmbarque : Tipo == "R" ? LimiteRevision : Tipo == "S" ? LimiteSalida:0;
            return ListaImageSource.Count < count;
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            service_Erecap.ObtenerFotos_Completed -= Service_ObtenerFotos_Completed;
            service_Erecap.SubirFotos_Completed -= Service_SubirFotos_Completed;
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("count"))
            {
                var count = (int)parameters["count"];
                if (count > 0)
                {
                    IsBusy = true;
                    service_Erecap.ObtenerFotos(Entrada, Tipo);
                }
                UpdateViewControls(count);
            }
        }

        public void OnNavigatingTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("count"))
            {
                var count = (int)parameters["count"];
                if (count > 0)
                {
                    IsBusy = true;
                    service_Erecap.ObtenerFotos(Entrada, Tipo);
                }
                UpdateViewControls(count);
            }
        }

        private ObservableCollection<ImageSource> _ListaImageSource;
        public ObservableCollection<ImageSource> ListaImageSource
        {
            get
            {
                if (_ListaImageSource == null)
                    _ListaImageSource = new ObservableCollection<ImageSource>();
                return _ListaImageSource;
            }
            set => SetProperty(ref _ListaImageSource, value);
        }
        private bool _HayFotos;
        public bool HayFotos
        {
            get => _HayFotos;
            set => SetProperty(ref _HayFotos, value);
        }

        private bool _NoHayFotos;

        public bool NoHayFotos
        {
            get => _NoHayFotos;
            set => SetProperty(ref _NoHayFotos, value);
        }

        public DelegateCommand SubirCommand { private set; get; }
        public DelegateCommand OpenGalleryCommand { private set; get; }
        public DelegateCommand ActualizarListaCommand { private set; get; }

        
    }
}
